using UnityEngine;
using System.Collections;

public class blockScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	  GetComponent<Collider2D>().enabled = false;
       GetComponent<Renderer>().sortingOrder = 1;
tag = "platform";
        //init monsters randomly
     int randMonster = Random.Range(0,11);
        if (randMonster==3 && gameObject.name=="platform(Clone)")
        {
        GameObject monster = Instantiate(Resources.Load("monster", typeof(GameObject))) as GameObject;
        Vector3 tmp = transform.position;
        tmp.y = transform.position.y + GetComponent<SpriteRenderer>().bounds.size.y/2+monster.GetComponent<SpriteRenderer>().bounds.size.y/2;
        monster.transform.position = tmp;
        monster.transform.parent = transform;
        monster.GetComponent<Renderer>().sortingOrder = 1;
        }
        //init bonus life on platform where are not monsters
         int randLife = Random.Range(0,30);
         if (randMonster!=3 && gameObject.name=="platform(Clone)" && randLife==2)
        {
        GameObject life = Instantiate(Resources.Load("life", typeof(GameObject))) as GameObject;
        Vector3 tmp = transform.position;
        tmp.y = transform.position.y + GetComponent<SpriteRenderer>().bounds.size.y/2+life.GetComponent<Collider2D>().bounds.size.y/2;
        life.transform.position = tmp;
        life.GetComponent<Renderer>().sortingOrder = 2;
        }
        //init shield  on platform where are not monsters and bonus life
        int randShield = Random.Range(0,30);
         if (randMonster!=3 && gameObject.name=="platform(Clone)" && randLife!=2 && randShield==3)
        {
        GameObject shield = Instantiate(Resources.Load("shield_icon", typeof(GameObject))) as GameObject;
        Vector3 tmp = transform.position;
        tmp.y = transform.position.y + GetComponent<SpriteRenderer>().bounds.size.y/2+shield.GetComponent<Collider2D>().bounds.size.y/2;
        shield.transform.position = tmp;
        shield.GetComponent<Renderer>().sortingOrder = 2;
        }
   
	}
	
	// Update is called once per frame
	void Update () {
          if( GameObject.Find("shield(Clone)"))
       {
           GetComponent<Collider2D>().enabled = false;
       }
       
        GameObject player = GameObject.Find("player");
        if (player && gameObject)
        {
	  if(player.transform.position.y - player.GetComponent<Collider2D>().bounds.size.y/1.5f>transform.position.y+GetComponent<Collider2D>().bounds.size.y/3f)
        {
            GetComponent<Collider2D>().enabled = true;
        }
            else
            {
                 GetComponent<Collider2D>().enabled = false;
            }
        }
       
	}
    void OnBecameInvisible() {
        Destroy(gameObject);
    }
    void OnBecameVisible()
    {
     
        
    }
}
