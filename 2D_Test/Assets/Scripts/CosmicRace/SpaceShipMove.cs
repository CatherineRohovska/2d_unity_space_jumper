using UnityEngine;
using System.Collections;

public class SpaceShipMove : MonoBehaviour {
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        //need to replace InputTouches.length
        float xTouch=0;
        float yTouch =0;
        float xCamPos = 0;
       if (Input.touches.Length!=0)
        {
     xTouch = Input.touches[0].position.x;
     xCamPos = Screen.width/2;
        }
	if(Input.GetKeyDown (KeyCode.LeftArrow)||xTouch<xCamPos)
    {
        moveLeft();
    
     }
            //move right
     if(Input.GetKeyDown (KeyCode.RightArrow)||xTouch>xCamPos)
    {
        moveRight();
   
    }
    if (Input.GetKeyUp(KeyCode.LeftArrow)||Input.GetKeyUp(KeyCode.RightArrow)||Input.touches.Length==0)
    {
        moveStop();    
    }
	}
  public  void moveLeft()
    {
        Vector3 tmp = new Vector3(-15.0f, 0.0f, 0);
        GetComponent<Rigidbody2D>().velocity = tmp;
     // StartCoroutine(StopAfterDelay(0.5f));
      //moveStop();
    }
 public   void moveRight()
    {
         Vector3 tmp = new Vector3(15.0f, 0.0f, 0);
        GetComponent<Rigidbody2D>().velocity = tmp;
   //  StartCoroutine(StopAfterDelay(0.5f));
    }
  
   
    void moveStop()
    {
         Vector3 tmp = new Vector3(0, 0.0f, 0);
        GetComponent<Rigidbody2D>().velocity = tmp;
    }
 
}
