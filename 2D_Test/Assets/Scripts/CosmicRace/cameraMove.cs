using UnityEngine;
using System.Collections;

public class cameraMove : MonoBehaviour {
    public float lastScreenWidth = 0f;
Camera cam;
    float xStart;
    float topStart;
    float brickHeight; 
int numRows; 
	// Use this for initialization
	void Start () {
        cam = Camera.main;
         AdjustScale();
 
        
        
	}
	
	// Update is called once per frame
	void Update () {
	if( lastScreenWidth != Screen.width ){
     lastScreenWidth = Screen.width;
     AdjustScale();
   }
	}
    void OnCollisionEnter2D(Collision2D coll) 
    {

        if(coll.gameObject.name == "arcanoid_platform")
        {
            print("collision enter");
            GameObject arcPlt = GameObject.Find("arcanoid_platform");
           Vector3 currVel =  arcPlt.GetComponent<Rigidbody2D>().velocity;
           currVel.y=0;              
         arcPlt.GetComponent<Rigidbody2D>().velocity = currVel;
          }
    }
    
 public   void InitLevel()
    {
     foreach (GameObject obj in GameObject.FindGameObjectsWithTag("block"))
     {
         Destroy(obj);
     }
        float koef = Mathf.Ceil((float)Screen.height/(float)Screen.width + 1/cam.orthographicSize);
        numRows = (int) Random.Range(2*koef,4*koef);
        print("rows: "+numRows+ " "+ koef);
        xStart = 0.47f;
    Vector2 pos = new Vector2(xStart, topStart); 
     GameObject block = Instantiate(Resources.Load("brick", typeof(GameObject))) as GameObject;
    brickHeight = block.GetComponent<SpriteRenderer>().bounds.size.y;
        pos.y = pos.y-brickHeight/2;
     block.transform.position = pos;
     block.GetComponent<Renderer>().sortingOrder = 1;  
        Destroy(block);
        int randExist;
        for (int i= 0; i<numRows; i++)
        {
           
            for(int j =0; j<10; j++)
            {
                randExist = Random.Range(0,2);
                if (randExist!=0) 
                {
            GameObject otherBlock = Instantiate(Resources.Load("brick", typeof(GameObject))) as GameObject;  
            otherBlock.transform.position = pos;
            otherBlock.tag = "block";
     otherBlock.GetComponent<Renderer>().sortingOrder = 1;  
            pos.x = pos.x+1;
                }
                
            }
            pos.y = pos.y-brickHeight;
            pos.x = xStart;
        }
    }
    
    void   AdjustScale(){
   // set up the other scene params.
        Time.timeScale=0;
         cam.orthographicSize = 5.0f/((float)Screen.width/(float)Screen.height);
        GameObject arcPlt = GameObject.Find("arcanoid_platform");
        GameObject edge = GameObject.Find("cameraChildEmpty");
         Vector3 p = cam.ViewportToWorldPoint(new Vector3(cam.rect.xMax, cam.rect.yMin, 4));
        p.x = p.x/2.0f;
        arcPlt.transform.position = p;
        GameObject ball = GameObject.Find("ball");
        Vector3 positionBall = cam.transform.position;
        positionBall.z = 4;
        ball.transform.position = positionBall;
        
     p = cam.ViewportToWorldPoint(new Vector3(cam.rect.xMin, cam.rect.yMax, 4));
         print(p.x+" "+p.y);
        edge.transform.position = p;
        topStart = p.y;
        ///
        InitLevel();
        Time.timeScale=1;
  }
}
