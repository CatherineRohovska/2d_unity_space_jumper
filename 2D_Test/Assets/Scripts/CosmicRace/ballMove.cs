using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ballMove : MonoBehaviour {
public Text scoreText;
float speedLimit = 5;
int formalCount;
int destroyedCount; 
string strText = "Blocks destroyed: ";    
	// Use this for initialization
	void Start () {
	 GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
        destroyedCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
	if (GetComponent<Rigidbody2D>().velocity.y>0)
    {
        GetComponent<Rigidbody2D>().gravityScale=0;
    }
    else
    {
        GetComponent<Rigidbody2D>().gravityScale=1;    
    }
	}
    void OnCollisionEnter2D(Collision2D coll) 
    {

           Vector3 currVel = GetComponent<Rigidbody2D>().velocity;

        if(Mathf.Abs(currVel.x)>speedLimit)
               currVel.x = currVel.x/Mathf.Abs(currVel.x) * speedLimit;
    
        if(Mathf.Abs(currVel.y)>speedLimit)
               currVel.y = currVel.y/Mathf.Abs(currVel.y) * speedLimit;
               
         GetComponent<Rigidbody2D>().velocity = currVel;
        
        if (coll.gameObject.name == "brick(Clone)")
        {
           UpdateScore();
        }
        
    }
    
     void OnBecameInvisible() {
   Destroy(gameObject);
   PlayerPrefs.SetInt("PrevScene", 2); 
   Application.LoadLevel ("GameOver"); 
         
            
    }
    void UpdateScore()
    {
        formalCount = GameObject.FindGameObjectsWithTag("block").Length-1;
        print("formal count"+formalCount);
        destroyedCount++;
        scoreText.text = strText + destroyedCount;
        if (formalCount==0)
        {
            print("formalCount = 0");
            Camera.main.GetComponent<cameraMove>().InitLevel();
            
          // Application.LoadLevel(Application.loadedLevel);  
        }
           int totalScore;
     if ( !PlayerPrefs.HasKey("ArcScore"))
     {
         totalScore = destroyedCount;
         PlayerPrefs.SetInt("ArcScore", totalScore);
     }
     else
     {
         totalScore = PlayerPrefs.GetInt("ArcScore");
         if (totalScore<destroyedCount)
         {
             totalScore = destroyedCount;
         PlayerPrefs.SetInt("ArcScore", totalScore);
         }
    }
}
}
