using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class playerScript : MonoBehaviour {
const float movementLength = 10.0f;
    //
 public Vector2 speed = new Vector2(50, 50);
 private Vector2 movement;
 private float inputY;
 private float inputX;
 private float posY; //camera position Y;
 private float positionPlayerY;    
 private float jumpHeight;
 private string scoreText = "Score: "; 
 private string lifesText = "Lifes: ";   
 public int lifesCount = 3;
    //
  public  Text txtScore;
  public  Text txtLifes;
private int currentscore=0;
 private Camera cam;
	// Use this for initialization
	void Start () {
        inputX = 0;
    cam = Camera.main;
  
    txtScore.text=scoreText+ currentscore; 
    txtLifes.text=lifesText+ lifesCount; 
	}
	
	// Update is called once per frame
	void Update () {
    // inputY = 0; 
     inputX=0;
//        float xTouch=0;
//        float yTouch =0;
//        float xCamPos = 0;
//        float yCamPos = 0;
//        int touchCount = 0;
//        if (Input.touches.Length!=0)
//        {
//     xTouch = Input.touches[0].position.x;
//     yTouch = Input.touches[0].position.y;
//     xCamPos = Screen.width/2;
//     yCamPos = Screen.height/2;
//     touchCount = Input.touches[0].tapCount;        
//        }
        //shooting
        if(Input.GetKeyDown (KeyCode.Space)|| Input.touches.Length!=0  )
    {
       //print (Input.touches.Length);
      ShotBullet();
//     xTouch=0;        
//    xCamPos = 0;   
    }
        //move left
    if(Input.GetKey(KeyCode.LeftArrow)||Input.acceleration.x<-0.25f)
    {
        moveLeft();
     
     }
  
          //move right
     if(Input.GetKey (KeyCode.RightArrow)||Input.acceleration.x>0.25f)
    {
        moveRight();
   
    }
   
    
          
    }
    
 public void moveLeft()
  {
     inputY = 0; 
     inputX=-movementLength/2;
       movement = new Vector2(
      speed.x * inputX,
      speed.y * inputY);
    
     

  }
public void moveRight()
 {
       inputY = 0; 
     inputX=movementLength/2;
      movement = new Vector2(
      speed.x * inputX,
      speed.y * inputY);
   

 }
    void FixedUpdate()
  {
    movement = new Vector2(
      speed.x * inputX,
      speed.y * inputY);
   Vector3 vel = GetComponent<Rigidbody2D>().velocity;
     vel.x = 0;
     GetComponent<Rigidbody2D>().velocity = vel;    
   GetComponent<Rigidbody2D>().AddForce(movement);
  }
    
 void OnCollisionEnter2D(Collision2D coll) {
        
       inputY = 0f; 
       inputX=0f;
     //adding force instead of bouncing
     if (coll.gameObject.name == "platform")
     {
         Debug.Log("Collision with platform");
movement = new Vector2(
      speed.x * 0,
      speed.y * 10);
         GetComponent<Rigidbody2D>().AddForce(movement);
 }
    // after collision reduce X-speed
   Vector3 vel = GetComponent<Rigidbody2D>().velocity;
     vel.x = 0;
     if (Mathf.Abs(vel.y)<1.6f && coll.gameObject.name == "platform(Clone)") 
     {
         movement = new Vector2(
      speed.x * 0,
      speed.y * 10);
         GetComponent<Rigidbody2D>().AddForce(movement);
     }
    if (!GameObject.Find("shield"))
     GetComponent<Rigidbody2D>().velocity = vel;
     //update score
     if ((int) transform.position.y*5 > currentscore  && coll.gameObject.name == "platform(Clone)" )
     {
     currentscore = (int) transform.position.y*5;
     }
     txtScore.text=scoreText+ currentscore;   
     //
   UpdateScore();
     //
    if (coll.gameObject.name == "monster(Clone)")
     {
        lifesCount--;
        txtLifes.text=lifesText+ lifesCount;
        Destroy(coll.gameObject);
        if (lifesCount==0)
        {
            Destroy(gameObject);
         Application.LoadLevel("GameOver");
        PlayerPrefs.SetInt("PrevScene", 1);
        }
    }
    }
void UpdateScore()
{
     int totalScore;
     if ( !PlayerPrefs.HasKey("JumperScore"))
     {
         totalScore = currentscore;
         PlayerPrefs.SetInt("JumperScore", totalScore);
     }
     else
     {
         totalScore = PlayerPrefs.GetInt("JumperScore");
         if (totalScore < currentscore)
         {
             totalScore = currentscore;
         PlayerPrefs.SetInt("JumperScore", totalScore);
         }
        // if (currentscore == 0) 
}
}
    void LateUpdate()
{
        //moving camera without motion on X axis
 Vector3 temp = cam.transform.position; 
 temp.x = 5.15f; 
 cam.transform.position = temp; 
   if (GetComponent<Rigidbody2D>().velocity.y<=0 || transform.position.y<cam.transform.position.y)
   {
    cam.transform.parent = GameObject.Find("Level").transform;
   }
        else
        {
            cam.transform.parent = GameObject.Find("player").transform;
        }
        
}
     void OnBecameInvisible()
     {
         Destroy(gameObject);
         Application.LoadLevel ("GameOver"); 
         UpdateScore();
         PlayerPrefs.SetInt("PrevScene", 1);
       
     }
    
 public  void ShotBullet()
    {//perform shot if there is no shield
         if( !GameObject.Find("shield(Clone)")&&!(GameObject.Find("bullet(Clone)")))
       {
        //calculating position of the nearest monster
        float minDistance=1000;
        Vector3 minPosition = new Vector3(0,0,0);
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("monster"))
        {           
            float distance = Mathf.Sqrt(Mathf.Pow((transform.position.x-obj.transform.position.x),2)+Mathf.Pow((transform.position.y-obj.transform.position.y),2));
            if (distance<minDistance) {minDistance=distance; minPosition = obj.transform.position;}
        }
        //creating bullet if monster exists
        if (minPosition.x!=0)
        {
          GameObject bullet = Instantiate(Resources.Load("bullet", typeof(GameObject))) as GameObject;
   
        bullet.transform.position = transform.position;
        bullet.GetComponent<Renderer>().sortingOrder = 2; 
            Vector3 tmp = GetComponent<Rigidbody2D>().velocity;
        tmp.y=3*(minPosition.y-transform.position.y);
        tmp.z=0;
        tmp.x=3*(minPosition.x-transform.position.x);
            float angle = Mathf.Atan2(tmp.y, tmp.x) * Mathf.Rad2Deg;
            if (angle>180) angle = 180-angle;
            bullet.transform.Rotate (Vector3.forward * angle);
            bullet.GetComponent<Rigidbody2D>().velocity = tmp;
            Destroy(bullet,2);
        }
            print("nearest monster: "+minPosition.x+" "+minDistance);
        }
    }
    
     void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.name == "life(Clone)" || other.gameObject.name == "life")
        {
         Debug.Log("Collision life with player");
        lifesCount++;
        txtLifes.text=lifesText + lifesCount;
        Destroy(other.gameObject);
        }
         if (other.gameObject.name == "shield_icon(Clone)" || other.gameObject.name == "shield_icon")
        {
             if (!GameObject.Find("shield(Clone)"))
             {
             Destroy(other.gameObject);
            GameObject shield = Instantiate(Resources.Load("shield", typeof(GameObject))) as GameObject;
            shield.transform.position = transform.position;
            shield.GetComponent<Renderer>().sortingOrder = 2;
             }
            
         }
    }

}

