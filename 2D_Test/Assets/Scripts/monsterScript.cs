using UnityEngine;
using System.Collections;

public class monsterScript : MonoBehaviour {
float velocity;

	// Use this for initialization
    GameObject parentObject;
    float leftSideOfBlock;
    float rightSideOfBlock;
	void Start () {

     parentObject = transform.parent.gameObject;
         Vector3 tmp = new Vector3(0.5f, 0, 0);
        GetComponent<Rigidbody2D>().velocity = tmp;
        float scaleXPlatform = parentObject. transform.localScale.x;
        leftSideOfBlock = parentObject.transform.position.x-parentObject.GetComponent<SpriteRenderer>().bounds.size.x/2.0f*scaleXPlatform;
            rightSideOfBlock = parentObject.transform.position.x+parentObject.GetComponent<SpriteRenderer>().bounds.size.x/2.0f*scaleXPlatform;
	}
	
	// Update is called once per frame
	void Update () {

        checkPositionAndChangeDirection();

	}
    void checkPositionAndChangeDirection()
    {Vector3 tmp = GetComponent<Rigidbody2D>().velocity;
        tmp.y=0;
        tmp.z=0;
       // tmp.x=3;
    float scaleXMonster = transform.localScale.x;
    float leftSidePosition =  
transform.position.x-GetComponent<SpriteRenderer>().bounds.size.x/2.0f;
     float rightSidePosition = transform.position.x+GetComponent<SpriteRenderer>().bounds.size.x/2.0f*scaleXMonster;
            if ( rightSidePosition>= rightSideOfBlock )
        {
            print("change velocity of monster");
            tmp.x = -1*tmp.x;
            GetComponent<Rigidbody2D>().velocity = tmp;
        }
     if(leftSidePosition <= leftSideOfBlock)
     {
         print("change velocity of monster");
            tmp.x = -1*tmp.x;
            GetComponent<Rigidbody2D>().velocity = tmp;
        
     }
	
    }
     void OnCollisionEnter2D(Collision2D coll) {
         if (coll.gameObject.name == "player")
         {
         Debug.Log("Collision player with monster");
    
         }
     }
    
    void OnBecameInvisible() {
    Destroy(gameObject);
    }
    void OnBecameVisible()
    {
        tag = "monster";
    }
    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.name == "bullet(Clone)")
        {
         Debug.Log("Collision bullet with monster");
          Destroy(gameObject);
            Destroy(other.gameObject);
        }
    }
}
