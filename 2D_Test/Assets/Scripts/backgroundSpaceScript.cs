using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class backgroundSpaceScript : MonoBehaviour {

    private float height;
    private float posY;
    private List<Vector3> positions;
    public bool isScale = false;
	// Use this for initialization
	void Start () {
	
    height = GetComponent<Renderer>().bounds.size.y;
    posY = transform.position.y;
        isScale = false;
        tag = "background";
	}
	void prepareCoordinates()
    {
        positions = new List<Vector3>();
  
        float heightMax = 2;
       
         int countVerticalBlocks = System.Convert.ToInt32(GetComponent<Renderer>().bounds.size.y)/System.Convert.ToInt32(heightMax);
         float randomX = Random.Range(0,GetComponent<Renderer>().bounds.size.x-6f);
        float randomY= transform.position.y-GetComponent<Renderer>().bounds.size.y/2+3f;

        print(countVerticalBlocks);
      for (int i = 0; i < countVerticalBlocks; i++)
      {
       randomY = randomY+1.8f;
          randomX = Random.Range(0,GetComponent<Renderer>().bounds.size.x-6f);
        Vector3 temp = new Vector3(randomX, randomY, 5); 
          positions.Add(temp);
          
             randomX = Random.Range(0,GetComponent<Renderer>().bounds.size.x-6f);
         temp = new Vector3(randomX+Random.Range(4f,6f), randomY+Random.Range(1.3f,1.5f), 0); 
        positions.Add(temp);
     
         temp = new Vector3(randomX+Random.Range(2f,3f), randomY+Random.Range(1.3f,1.5f), 0); 
        positions.Add(temp);
      }
    
    }
        
	// Update is called once per frame
	void Update () {
      
	
	}
     void OnBecameInvisible() {
        
       Vector3 temp = transform.position;
         temp.y = posY+3.0f*height;
         transform.position = temp;
      
    }
    void OnBecameVisible() {
        posY = transform.position.y;
       
       addBlocks();
       
        //adding blocks
       
    }
    
    void addBlocks()
    {
        prepareCoordinates();
    
     
        foreach (Vector3 temp in positions)
        {
        GameObject block = Instantiate(Resources.Load("platform", typeof(GameObject))) as GameObject;
   
        block.transform.position = temp;
           

        block.GetComponent<Renderer>().sortingOrder = 1;
        }
   
    }
}
